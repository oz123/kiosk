#!/bin/bash
# Copyright (C) 2019 noris netwrok AG
# Copyright (C) 2020 Oz Tiram

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
set -e
# --------------------------------------------------------------------------------------------------------------
# We are explicitly not using a templating language to inject the values as to encourage the user to limit their
# set of templating logic in these files. By design all injected values should be able to be set at runtime,
# and the shell script real work. If you need conditional logic, write it in bash or make another shell script.
# --------------------------------------------------------------------------------------------------------------

# ONLY CHANGE VERSIONS HERE IF YOU KNOW WHAT YOU ARE DOING!
# MAKE SURE THIS MATCHED THE MASTER K8S VERSION
# load kiosk environment file if available
if [ -f /etc/kubernetes/kiosk.env ]; then
    # shellcheck disable=SC1091
    source /etc/kubernetes/kiosk.env
fi

LOGFILE=/dev/stderr
export DEBIAN_FRONTEND="noninteractive"

function log() {
    datestring=`date +"%Y-%m-%d %H:%M:%S"`
    echo -e "$datestring - $@" | tee $LOGFILE
}

KUBE_VERSION_COMPARE="$(echo "${KUBE_VERSION}" | cut -d '.' -f 2 )"

export KUBE_VERSION=${KUBE_VERSION:-1.18.3}
export DOCKER_VERSION=${DOCKER_VERSION:-"1.19"}
TRANSPORT_PACKAGES="apt-transport-https ca-certificates curl software-properties-common gnupg2"

iptables -P FORWARD ACCEPT
swapoff -a


# bootstrap the node
cat << EOF > /etc/kubernetes/cluster-info.yaml
---
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${B64_CA_CONTENT}
    server: https://${LOAD_BALANCER_DNS:-${LOAD_BALANCER_IP}}:${LOAD_BALANCER_PORT}
  name: ""
contexts: []
current-context: ""
kind: Config
preferences: {}
users: []
EOF

cat << EOF > /etc/kubernetes/kubeadm-node-"${KUBE_VERSION}".yaml
---
apiVersion: kubeadm.k8s.io/v1beta2
discovery:
  bootstrapToken:
    apiServerEndpoint: "${LOAD_BALANCER_DNS:-${LOAD_BALANCER_IP}}:${LOAD_BALANCER_PORT}"
    token: ${BOOTSTRAP_TOKEN}
    caCertHashes:
     - "sha256:${DISCOVERY_HASH}"
    unsafeSkipCAVerification: false
  timeout: 15m0s
kind: JoinConfiguration
nodeRegistration:
  criSocket: /var/run/dockershim.sock
---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
serverTLSBootstrap: true
EOF

function get_kubeadm {
    log "started ${FUNCNAME[0]}"
    curl --retry 10 -fssL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    apt-add-repository -u "deb http://apt.kubernetes.io kubernetes-xenial main"
    apt-get install -y --allow-change-held-packages --allow-downgrades kubeadm=${KUBE_VERSION}-00 kubelet=${KUBE_VERSION}-00
    log "Finished ${FUNCNAME[0]}"
}

function get_docker() {
    log "Started get_docker"
    curl --retry 10 -fssl https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    apt-get update
    apt-get install -y socat conntrack ipset
    apt-get update && apt-get install -y \
       containerd.io=1.2.13-2 \
       docker-ce=5:19.03.11~3-0~ubuntu-$(lsb_release -cs) \
       docker-ce-cli=5:19.03.11~3-0~ubuntu-$(lsb_release -cs)
    cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
    mkdir -p /etc/systemd/system/docker.service.d
    # Restart Docker
    systemctl daemon-reload
    systemctl restart docker
    systemctl enable docker
    log "Finished ${FUNCNAME[0]}"
}

# run commands needed for network plugins
function config_pod_network(){
    case "${POD_NETWORK}" in
        "CALICO")
            ;;
        "FLANNEL")
            sysctl net.bridge.bridge-nf-call-iptables=1
            ;;
    esac
}

function join() {
    kubeadm -v=10 join --config /etc/kubernetes/kubeadm-node-"${KUBE_VERSION}".yaml "${LOAD_BALANCER_DNS:-${LOAD_BALANCER_IP}}:${LOAD_BALANCER_PORT}"
}

function fetch_all() {
    apt-get update
    dpkg -l software-properties-common | grep ^ii || apt-get install ${TRANSPORT_PACKAGES} -y

    if [ -z "$(type -P docker)" ]; then
        get_docker
        for i in $(seq 1 10); do get_docker && break; sleep 30; done;
    fi
    for i in $(seq 1 10); do get_kubeadm && break; sleep 30; done;
}

# misc fixes for different problems which might be needed or not
function fixes() {
    # https://support.binarylane.com.au/support/solutions/articles/11000095534-ubuntu-20-04-upgrade-error-for-grub-efi-amd64-signed
    sed -Ei 's/^(GRUB_CMDLINE_LINUX_DEFAULT=)"[^"]*"/\1"quiet nosplash net.ifnames=0 vga=770"/' /etc/default/grub && update-grub
    apt remove -y grub-efi-amd64-signed
}

function main() {

    fixes

    kubeadm version | grep -qi "${KUBE_VERSION}" || fetch_all
    config_pod_network

    # join !
    until join; do sudo kubeadm reset --force
    done
}

# This line and the if condition bellow allow sourcing the script without executing
# the main function
(return 0 2>/dev/null) && sourced=1 || sourced=0

if [[ $sourced == 1 ]]; then
    set +e
    echo "You can now use any of these functions:"
    echo ""
    typeset -F |  cut -d" " -f 3
else
    set -eu
    cd /root
    iptables -P FORWARD ACCEPT
    swapoff -a
    main "$@"
fi

# vi: ts=4 sw=4 ai
