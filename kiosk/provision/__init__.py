# Copyright (C) 2019 noris netwrok AG
# Copyright (C) 2020 Oz Tiram

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

.. _userdata:

kiosk.provison.userdata
-----------------------

bootstrap-k8s-master-ubuntu-16.04.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This script is executed on the first master on cluster boot.
One can also run this script on bare metal machines to create
a ``kubeadm`` based kubernetes cluster.

The script needs ``/etc/kubernetes/kiosk.env`` to run properly.
This file includes all the information on the cluster and
written via ``cloud-init`` on the first boot.
See :py:class:`kiosk.provision.cloud_init.FirstMasterInit`

.. literalinclude:: ../kiosk/provision/userdata/bootstrap-k8s-master-ubuntu-16.04.sh
   :language: shell

bootstrap-k8s-nth-master-ubuntu-16.04.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This script is executed on the nth master upon initial bootstrap.

.. literalinclude:: ../kiosk/provision/userdata/bootstrap-k8s-nth-master-ubuntu-16.04.sh
   :language: shell

bootstrap-k8s-node-ubuntu-16.04.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. literalinclude:: ../kiosk/provision/userdata/bootstrap-k8s-node-ubuntu-16.04.sh
   :language: shell

"""
