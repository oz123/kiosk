# Copyright (C) 2019 noris netwrok AG
# Copyright (C) 2020 Oz Tiram

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This modules contains some helper functions to inject cloud-init
to booted machines. At the moment only Cloud Inits for Ubunut 20.04 are
provided
"""
import base64
import os
import sys
import textwrap

from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from pkg_resources import (Requirement, resource_filename)

import yaml

from cryptography.hazmat.primitives import serialization

from kiosk import __version__, KUBERNETES_BASE_VERSION
from kiosk.ssl import b64_cert, b64_key
from kiosk.util.logger import Logger
from kiosk.util.util import get_kubeconfig_yaml

LOGGER = Logger(__name__)


BOOTSTRAP_SCRIPTS_DIR = "/kiosk/provision/userdata/"


def read_encode_base64(filepath):
    with open(filepath, "br") as fh:
        mat = b''.join(fh.readlines())
        enc = base64.b64encode(mat)
    return enc


def get_audit_policy():
    """read the auditpolicy.yml"""
    if getattr(sys, 'frozen', False):
        path = os.path.join(
            sys._MEIPASS,  # pylint: disable=no-member, protected-access
            'provision/userdata/manifests/audit-policy.yml')
    else:
        path = resource_filename(Requirement('kiosk'),
                                 os.path.join(BOOTSTRAP_SCRIPTS_DIR,
                                              'manifests',
                                              'audit-policy.yml'))
    with open(path) as policy:
        return policy.read()


class BaseInit:  # pylint: disable=unnecessary-lambda,no-member
    """
    Args:
       cloud_config   An OSCloudConfig instance describing the information
                      necessary for sending requests to the underlying cloud.
                      Needed e.g. for auto scaling.

    Attributes:
        cloud_config_data       this attribute contains the text/cloud-config
                                files that is passed to the instances
        attachments             this attribute contains other parts of the
                                userdata, e.g scripts that are directly
                                executed by cloud-init. They should be
                                instances of MIMEText with the header
                                'Content-Disposition' set to 'attachment'
    """
    def __init__(self, cloud_config):
        self.cloud_config = cloud_config

        self._cloud_config_data = {}
        self._attachments = []

        # if needed we can declare and use other sections at this point...
        self._cloud_config_data['write_files'] = []
        self._cloud_config_data['runcmd'] = []
        self._cloud_config_data['runcmd'].append('swapoff -a')

        # assemble the parts
        self._write_kiosk_info()

    def add_scripts(self):
        self.add_bootstrap_script()

    def write_file(self, path, content, owner="root", group="root",
                   permissions="0600", encoder=lambda x: base64.b64encode(x)):
        """
        writes a file to the instance
        path: e.g. /etc/kubernetes/kiosk.conf
        content: string of the content of the file
        owner: e.g. root
        group: e.g. root
        permissions: e.g. "0644", as string
        encode: Optional encoder to use for the needed base64 encoding
        """
        data = {
            "path": path,
            "owner": owner + ":" + group,
            "encoding": "b64",
            "permissions": permissions,
            "content": encoder(content.encode()).decode()
        }
        self._cloud_config_data['write_files'].append(data)

    def add_bootstrap_script(self):
        """
        add a bootstrap script to each cluster member.
        """
        name, script = self._get_script(
            "bootstrap-k8s-%s-%s-%s.sh" % (
                self.role, self.os_type, self.os_version))
        part = MIMEText(script, _subtype='x-shellscript')
        part.add_header('Content-Disposition', 'attachment',
                        filename=name)
        self._attachments.append(part)

    def _get_script(self, name):

        if getattr(sys, 'frozen', False):
            path = os.path.join(
                sys._MEIPASS,  # pylint: disable=no-member, protected-access
                'provision/userdata', name)
        else:
            path = resource_filename(Requirement('kiosk'),
                                     os.path.join(BOOTSTRAP_SCRIPTS_DIR,
                                                  name))
        with open(path) as fh:
            script = fh.read()

        return name, script

    def add_ssh_public_key(self, ssh_key):
        """
        ssh_key istance of ``cryptography.hazmat.backends.openssl.rsa._RSAPrivateKey``
        """
        if isinstance(ssh_key, str):
            keyline = ssh_key
        else:
            keyline = ssh_key.public_key().public_bytes(
                serialization.Encoding.OpenSSH,
                serialization.PublicFormat.OpenSSH).decode()

        self._cloud_config_data["ssh_authorized_keys"] = []
        self._cloud_config_data["ssh_authorized_keys"].append(keyline)

    def _write_kiosk_info(self):
        """
        Generate the kiosk.conf configuration file.
        """
        content = """
        # This file contains meta information about kiosk
        kiosk_version={}
        creation_date={}
        """.format(__version__,
                   datetime.strftime(datetime.now(), format="%c"))
        content = textwrap.dedent(content)

        self.write_file("/etc/kubernetes/kiosk.conf",
                        content,
                        "root",
                        "root",
                        "0644")

    def _write_cloud_config(self):
        """
        write out the cloud provider configuration file for OpenStack
        """
        content = str(self.cloud_config)
        self.write_file("/etc/kubernetes/cloud.config", content, "root",
                        "root", "0600")

    def __str__(self):
        """
        This method generates a string from the cloud_config_data and the
        attachments that have been set in the corresponding attributes.
        """
        # all the other parts of the cloud init script are already
        # create when creating the object's instance. When we call
        # add_bootstrap_script it is appended to self._attachment
        self.add_scripts()
        userdata = MIMEMultipart()

        # first add the cloud-config-data script
        config = MIMEText(yaml.dump(self._cloud_config_data),
                          _subtype='cloud-config')
        config.add_header('Content-Disposition', 'attachment')
        userdata.attach(config)

        for attachment in self._attachments:
            userdata.attach(attachment)

        return userdata.as_string()


class NthMasterInit(BaseInit):
    """
    Initialization userdata for an n-th master node. Nothing more than
    adding an public SSH key for access from the first master node needs
    to be done.
    """
    def __init__(  # pylint: disable=too-many-arguments
            self,
            cloud_config,
            ssh_key,
            os_type='ubuntu',
            os_version="20.04",
            kiosk_env=None,
            k8s_conf=None,
            ca_bundle=None):
        """
        ssh_key is a RSA keypair (return value from create_key from util.ssl
            package)

        Args:
            k8s_conf (str) - path the the k8s configuration file
        """
        super().__init__(cloud_config)
        self.ssh_key = ssh_key
        self.os_type = os_type
        self.os_version = os_version
        self.role = 'nth-master'
        self.kiosk_env = kiosk_env
        self._write_kiosk_env()

        if k8s_conf:
            encode_k8s_conf = get_kubeconfig_yaml(k8s_conf.host,
                                                  read_encode_base64(k8s_conf.ssl_ca_cert),
                                                  'admin',
                                                  read_encode_base64(k8s_conf.cert_file),
                                                  read_encode_base64(k8s_conf.key_file),
                                                  )

            self.write_file("/etc/kubernetes/admin.conf",
                            encode_k8s_conf)

        # write audit policy
        self.write_file("/etc/kubernetes/audit-policy.yml", get_audit_policy())

        # assemble the parts for an n-th master node
        if self.ssh_key:
            self.add_ssh_public_key(self.ssh_key)
            # assemble the parts for the first master
            # use an encoder that just returns x, since b64_cert encodes already
            # in base64 mode
            self.write_file("/etc/kubernetes/pki/ca.crt",
                            b64_cert(ca_bundle.cert),
                            "root", "root", "0600", lambda x: x)
            self.write_file("/etc/kubernetes/pki/ca.key",
                            b64_key(ca_bundle.key),
                            "root", "root", "0600", lambda x: x)

    def _write_kiosk_env(self):
        """
        writes the necessary kiosk information for the node to the file
        /etc/kubernetes/kiosk.env
        """

        kenv = self.kiosk_env
        if not kenv:
            raise ValueError("kiosk_env dictionary can't be empty")

        def gk(key, dic=kenv, default=""):  # pylint: disable=invalid-name
            """Retrieves a key from a dictionary with a default."""
            val = dic.get(key, default)

            if val is None:
                return default
            return val

        content = f"""
            #!/bin/bash
            export MASTERS_IPS=( {" ".join(gk('master_ips'))} )
            export MASTERS=( {" ".join(gk('master_names'))} )
            export AUTO_JOIN="{gk('auto_join')}"
            export CURRENT_CLUSTER="{gk('current_cluster')}"

            export LOAD_BALANCER_DNS="{gk('lb_dns')}"
            export LOAD_BALANCER_IP="{gk('lb_ip')}"
            export LOAD_BALANCER_PORT="{gk('lb_port')}"

            export BOOTSTRAP_TOKEN="{gk('bootstrap_token')}"

            export POD_SUBNET="{gk('pod_subnet')}"
            export POD_NETWORK="{gk('pod_network')}"

            export KUBE_VERSION="{gk('k8s_version')}"
        """
        content = textwrap.dedent(content)

        content = textwrap.dedent(content)

        self.write_file("/etc/kubernetes/kiosk.env", content, "root", "root",
                        "0600")


# pylint: disable=too-many-arguments,too-many-locals
class FirstMasterInit(NthMasterInit):
    """
    This node executes the bootstrap strip to create the initial cluster.

    Args:
        ssh_key (RSAkeypair) - an RSA keypair instance from
                :func:`~kiosk.ssl.create_key`
        ca_bundle: The CA bundle for the CA that is used to permit accesses
            to the API server.
        cloud_config: An OSCloudConfig instance describing the information
            necessary for sending requests to the underlying cloud.
        os_type (str): the OS type the bootstrap script runs on
        os_version (str): OS version the bootstrap script runs on
        kiosk_env (dict): A dictionary containing information for the
            kiosk.env

    """

    def __init__(self,
                 ssh_key,
                 ca_bundle,
                 cloud_config,
                 os_type='ubuntu',
                 os_version="20.04",
                 kiosk_env=None):

        super().__init__(
                cloud_config,
                ssh_key,
                os_type,
                os_version,
                kiosk_env=kiosk_env,
                ca_bundle=ca_bundle)
        self.ca_bundle = ca_bundle
        self.role = 'master'

        self._write_cloud_config()
        self._write_ssh_private_key()

    def _write_ssh_private_key(self):
        # path = "/home/{}/.ssh/id_rsa_masters".format(self.username)
        key = self.ssh_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption()).decode()

        self._cloud_config_data["ssh_keys"] = {}
        self._cloud_config_data["ssh_keys"]["rsa_private"] = key


class SingleMasterInit(FirstMasterInit):

    def __init__(self, ssh_key, ca_bundle, cloud_config,
            os_type='ubuntu', os_version="20.04",
            kiosk_env=None):
        super().__init__(ssh_key,
                ca_bundle,
                cloud_config,
                os_type,
                os_version,
                kiosk_env=kiosk_env)

        self.write_file("/etc/kiosk/nginx-snippet.conf",
                self.create_nginx_snippet(),
                "root",
                "root",
                "0600",
                )

    def create_nginx_snippet(self):
        # kiosk_env  contains the info about upstreams
        config = "max_fails=3 fail_timeout=5s;"
        http_port = self.kiosk_env.get('http_port')
        https_port = self.kiosk_env.get('https_port')
        servers = "\n".join("%s:%s %s" % (
            "server ".rjust(19) + name, http_port, config) for name in
            (self.kiosk_env.get('master_names') + self.kiosk_env.get('node_names'))
        )
        tls_servers = "\n".join("%s:%s %s" % (
            "server ".rjust(19) + name, https_port, config) for name in
            (self.kiosk_env.get('master_names') + self.kiosk_env.get('node_names'))
        )
        base = textwrap.dedent("""
        # do not edit, managed by kiosk
        stream {
            upstream nginx_http {
            least_conn;
%s
            }
            server {
                listen 80;
                proxy_pass nginx_http;
                proxy_protocol on;
            }

            upstream nginx_https {
            least_conn;
%s
            }
            server {
                listen     443;
                proxy_pass nginx_https;
                proxy_protocol on;
            }

        }
        """ % (servers, tls_servers)
        )
        return base

    def _add_nginx_config(self):
        name, script = self._get_script("configure-nginx-proxy.sh")
        part = MIMEText(script, _subtype='x-shellscript')
        part.add_header('Content-Disposition', 'attachment',
                        filename=name)
        self._attachments.append(part)

    def add_scripts(self):
        self.add_bootstrap_script()
        self._add_nginx_config()


class NodeInit(BaseInit):
    """
    The node does nothing else than executing its bootstrap script.
    """
    def __init__(self, ca_cert, cloud_config, lb_ip, lb_port, bootstrap_token,
                 discovery_hash, lb_dns='', os_type='ubuntu',
                 os_version="20.04",
                 k8s_version=KUBERNETES_BASE_VERSION,
                 pod_network="CALICO"):
        """
        """
        super().__init__(cloud_config)
        self.ca_cert = ca_cert
        self.lb_ip = lb_ip
        self.lb_port = lb_port
        self.bootstrap_token = bootstrap_token
        self.discovery_hash = discovery_hash
        self.lb_dns = lb_dns
        self.os_type = os_type
        self.os_version = os_version
        self.role = "node"
        self.k8s_version = k8s_version
        self.pod_network = pod_network

        # assemble parts for the node
        self._write_kiosk_env()
        # TODO: this is no longer needed on the nodes with CSI-Cinder
        # Verify this!!!
        # self._write_cloud_config()

    def _write_kiosk_env(self):
        """
        writes the necessary kiosk information for the node to the file
        /etc/kubernetes/kiosk.env
        """
        content = """
            #!/bin/bash
            export B64_CA_CONTENT="{}"
            export LOAD_BALANCER_DNS="{}"
            export LOAD_BALANCER_IP="{}"
            export LOAD_BALANCER_PORT="{}"
            export BOOTSTRAP_TOKEN="{}"
            export DISCOVERY_HASH="{}"
            export KUBE_VERSION="{}"
            export POD_NETWORK="{}"
        """.format(b64_cert(self.ca_cert), self.lb_dns, self.lb_ip,
                   self.lb_port, self.bootstrap_token, self.discovery_hash,
                   self.k8s_version,
                   self.pod_network)
        content = textwrap.dedent(content)
        self.write_file("/etc/kubernetes/kiosk.env", content, "root", "root",
                        "0600")
