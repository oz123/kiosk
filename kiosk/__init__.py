# Copyright (C) 2019 noris netwrok AG
# Copyright (C) 2020 Oz Tiram

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# pylint: disable=missing-docstring
try:
    import pkg_resources
    __version__ = pkg_resources.get_distribution('kiosk').version
except pkg_resources.DistributionNotFound:
    __version__ = '1.3.4'

# Defining some constants
MASTER_PREFIX = "master"
MASTER_LISTENER_NAME = f"{MASTER_PREFIX}-listener"
MASTER_POOL_NAME = f"{MASTER_PREFIX}-pool"
KUBERNETES_BASE_VERSION = "1.14.1"
