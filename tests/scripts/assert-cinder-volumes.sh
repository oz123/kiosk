#!/bin/bash

set -e

ERR=0

function __cleanup() {
	kubectl --kubeconfig=${KUBECONFIG} delete -f tests/integration/topology-aware.yml
	kubectl --kubeconfig=${KUBECONFIG} delete -f tests/integration/pvcs.yml
	if [ ${ERR} -ne 0 ]; then
		exit 1
	fi
}

function apply() {
	kubectl --kubeconfig=${KUBECONFIG} apply -f $1
}

function test_cinder(){
	apply tests/integration/default-storageClass-de-nbg6-1a.yml
	apply tests/integration/default-storageClass-de-nbg6-1b.yml
	apply tests/integration/pvcs.yml

	echo "Waiting for volumes to bind"

	until ( kubectl --kubeconfig=${KUBECONFIG} get pvc | grep -ci bound > /dev/null) do
		echo -n ".";
		sleep 1
	done

	echo ""

	if [ $(kubectl --kubeconfig=${KUBECONFIG} get pvc | grep -ci bound > /dev/null) -ne 2 ]; then
		echo "Regular cinder volumes failed to bind"
		ERR=1
	fi
}


function test_cinder_topology() {

	sed -i 's/%%ZONES%%/'"${AVAILABILITY_ZONES}"'/' tests/integration/topology-aware.yml

	apply tests/integration/topology-aware.yml

	echo "Waiting for volume to bind"
	until ( kubectl --kubeconfig=${KUBECONFIG} get pvc pvc-topology | grep -ci bound > /dev/null) do
		echo -n ".";
		sleep 1;
	done

	echo ""

	if ! kubectl --kubeconfig=${KUBECONFIG} get pv -o=jsonpath='{.items[0].metadata.annotations.pv\.kubernetes\.io/provisioned-by}' | grep cinder.csi.openstack.org; then
		echo "Topology aware cinder volume failed"
		ERR=1
	fi
}

trap __cleanup EXIT

test_cinder
test_cinder_topology
